import * as React from 'react';
import { ReactNode } from 'react';
import { ShapeSizeType } from './data/ShapeSizes';

interface CircleProps {
  children?: ReactNode;
  size: ShapeSizeType;
}

const Circle: (props: CircleProps) => JSX.Element = (props) => {
  return <div className={'circle-' + props.size}>{props.children}</div>;
};
export default Circle;
