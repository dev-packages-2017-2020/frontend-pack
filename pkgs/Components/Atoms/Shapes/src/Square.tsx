import * as React from 'react';
import { ReactNode } from 'react';
import { ShapeSizeType } from './data/ShapeSizes';

interface SquareProps {
  children?: ReactNode;
  size: ShapeSizeType;
}

const Square: (props: SquareProps) => JSX.Element = (props) => {
  return <div className={'square-' + props.size}>{props.children}</div>;
};
export default Square;
