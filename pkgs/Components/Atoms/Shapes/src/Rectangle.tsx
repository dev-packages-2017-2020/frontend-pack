import * as React from 'react';
import { ReactNode } from 'react';
import { ShapeSizeType } from './data/ShapeSizes';

interface RectangleProps {
  children?: ReactNode;
  width: ShapeSizeType;
  height: ShapeSizeType;
}

const Rectangle: (props: RectangleProps) => JSX.Element = (props) => {
  return <div className={'h-rectangle-' + props.width + ' v-rectangle-' + props.height}>{props.children}</div>;
};
export default Rectangle;
