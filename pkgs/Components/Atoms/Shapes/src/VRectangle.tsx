import * as React from 'react';
import { ReactNode } from 'react';
import { ShapeSizeType } from './data/ShapeSizes';

interface VRectangleProps {
  children?: ReactNode;
  size: ShapeSizeType;
}

const VRectangle: (props: VRectangleProps) => JSX.Element = (props) => {
  return <div className={'v-rectangle-' + props.size}>{props.children}</div>;
};
export default VRectangle;
