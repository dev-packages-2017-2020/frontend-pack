import * as React from 'react';
import { ReactNode } from 'react';
import { ShapeSizeType } from './data/ShapeSizes';

interface HRectangleProps {
  children?: ReactNode;
  size: ShapeSizeType;
}

const HRectangle: (props: HRectangleProps) => JSX.Element = (props) => {
  return <div className={'h-rectangle-' + props.size}>{props.children}</div>;
};
export default HRectangle;
