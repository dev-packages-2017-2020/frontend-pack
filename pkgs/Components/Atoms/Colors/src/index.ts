import { BackgroundColors, BackgroundColorType } from './data/BackgroundColors';
import BasicColor from './Color';

export { BackgroundColors, BackgroundColorType, BasicColor };
