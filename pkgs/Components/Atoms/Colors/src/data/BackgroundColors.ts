export type BackgroundColorType =
  | 'bg-color-1'
  | 'bg-color-2'
  | 'bg-color-3'
  | 'bg-color-4'
  | 'bg-color-5'
  | 'bg-color-success'
  | 'bg-color-validate'
  | 'bg-color-error'
  | 'bg-color-cancel'
  | 'bg-color-1 shadowed'
  | 'bg-color-2 shadowed'
  | 'bg-color-3 shadowed'
  | 'bg-color-4 shadowed'
  | 'bg-color-5 shadowed'
  | 'bg-color-success shadowed'
  | 'bg-color-validate shadowed'
  | 'bg-color-error shadowed'
  | 'bg-color-cancel shadowed'
  | 'bg-secondary-color-1'
  | 'bg-secondary-color-2'
  | 'bg-secondary-color-3'
  | 'bg-secondary-color-4'
  | 'bg-secondary-color-5'
  | 'bg-secondary-color-success'
  | 'bg-secondary-color-validate'
  | 'bg-secondary-color-error'
  | 'bg-secondary-color-cancel'
  | 'bg-secondary-color-1 shadowed'
  | 'bg-secondary-color-2 shadowed'
  | 'bg-secondary-color-3 shadowed'
  | 'bg-secondary-color-4 shadowed'
  | 'bg-secondary-color-5 shadowed'
  | 'bg-secondary-color-success shadowed'
  | 'bg-secondary-color-validate shadowed'
  | 'bg-secondary-color-error shadowed'
  | 'bg-secondary-color-cancel shadowed';

const BackgroundColors: BackgroundColorType[] = [
  'bg-color-1',
  'bg-color-2',
  'bg-color-3',
  'bg-color-4',
  'bg-color-5',
  'bg-color-success',
  'bg-color-validate',
  'bg-color-error',
  'bg-color-cancel',
  'bg-color-1 shadowed',
  'bg-color-2 shadowed',
  'bg-color-3 shadowed',
  'bg-color-4 shadowed',
  'bg-color-5 shadowed',
  'bg-color-success shadowed',
  'bg-color-validate shadowed',
  'bg-color-error shadowed',
  'bg-color-cancel shadowed',
  'bg-secondary-color-1',
  'bg-secondary-color-2',
  'bg-secondary-color-3',
  'bg-secondary-color-4',
  'bg-secondary-color-5',
  'bg-secondary-color-success',
  'bg-secondary-color-validate',
  'bg-secondary-color-error',
  'bg-secondary-color-cancel',
  'bg-secondary-color-1 shadowed',
  'bg-secondary-color-2 shadowed',
  'bg-secondary-color-3 shadowed',
  'bg-secondary-color-4 shadowed',
  'bg-secondary-color-5 shadowed',
  'bg-secondary-color-success shadowed',
  'bg-secondary-color-validate shadowed',
  'bg-secondary-color-error shadowed',
  'bg-secondary-color-cancel shadowed',
];

export { BackgroundColors };
