import * as React from 'react';
import { ReactNode } from 'react';
import { BackgroundColorType } from './data/BackgroundColors';

interface ColorProps {
  children?: ReactNode;
  color: BackgroundColorType;
}

const BasicColor: (props: ColorProps) => JSX.Element = (props) => {
  return <div className={props.color + ' full-div'}>{props.children}</div>;
};
export default BasicColor;
