const randomInt: (N: number) => number = (N) => Math.floor(Math.random() * N);
const randomFloat: (N: number) => number = (N) => Math.random() * N;
const randomBool: () => boolean = () => Math.random() < 0.5;

const randomAny: () => any = () => {
  const v = Math.floor(Math.random() * 3);
  if (v === 0) {
    return randomInt(100);
  }
  if (v === 1) {
    return randomFloat(100).toFixed(2);
  }
  return randomBool();
};

export { randomInt, randomFloat, randomBool, randomAny };
