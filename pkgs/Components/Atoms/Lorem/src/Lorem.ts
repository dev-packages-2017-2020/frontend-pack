import { LoremIpsum } from 'lorem-ipsum';

/**
 * @usage
 * lorem.generateWords(1);
 * lorem.generateSentences(5);
 * lorem.generateParagraphs(7);
 *
 * @type {LoremIpsum}
 */
const lorem = new LoremIpsum({
  sentencesPerParagraph: {
    max: 4,
    min: 2,
  },
  wordsPerSentence: {
    max: 16,
    min: 4,
  },
});
export default lorem;
