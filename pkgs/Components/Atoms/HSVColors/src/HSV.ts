import RGB, { RGBType } from './RGB';

export interface HSVType {
  h: number;
  s: number;
  v: number;
  display: () => string;
  toRGB: () => RGBType;
  toHex: () => string;
  luminance: () => number;
  complementaryColor: () => HSVType;
}

interface RGBDictionary {
  [index: string]: RGBType;
}

class HSV implements HSVType {
  h: number;
  s: number;
  v: number;

  static rgbCache: RGBDictionary = {};

  constructor(h: number, s: number, v: number) {
    this.h = h;
    this.s = s;
    this.v = v;
  }

  display() {
    return (
      'HSV(' +
      this.h +
      ',' +
      Math.floor(this.s * 100) +
      ',' +
      Math.floor(this.v * 100) +
      ')' +
      ' -- RGB(' +
      this.toRGB().r +
      ',' +
      this.toRGB().g +
      ',' +
      this.toRGB().b +
      ')' +
      ' -- Hex : ' +
      this.toHex() +
      ' -- HexComplement : ' +
      this.complementaryColor().toHex()
    );
  }

  toRGB() {
    const cacheKey = this.h + '-' + this.s + '-' + this.v;
    if (HSV.rgbCache[cacheKey]) return HSV.rgbCache[cacheKey];

    const ti = Math.floor(this.h / 60) % 6;
    const f = this.h / 60 - ti;
    let l = this.v * (1 - this.s);
    let m = this.v * (1 - f * this.s);
    let n = this.v * (1 - (1 - f) * this.s);
    l = Math.floor(l * 255);
    m = Math.floor(m * 255);
    n = Math.floor(n * 255);
    const v = Math.floor(this.v * 255);
    switch (ti) {
      case 0:
        HSV.rgbCache[cacheKey] = new RGB(v, n, l);
        return HSV.rgbCache[cacheKey];
      case 1:
        HSV.rgbCache[cacheKey] = new RGB(m, v, l);
        return HSV.rgbCache[cacheKey];
      case 2:
        HSV.rgbCache[cacheKey] = new RGB(l, v, n);
        return HSV.rgbCache[cacheKey];
      case 3:
        HSV.rgbCache[cacheKey] = new RGB(l, m, v);
        return HSV.rgbCache[cacheKey];
      case 4:
        HSV.rgbCache[cacheKey] = new RGB(n, l, v);
        return HSV.rgbCache[cacheKey];
      case 5:
        HSV.rgbCache[cacheKey] = new RGB(v, l, m);
        return HSV.rgbCache[cacheKey];
    }
    return new RGB(0, 0, 0);
  }

  toHex() {
    return this.toRGB().toHex();
  }

  luminance() {
    return this.toRGB().luminance();
  }

  _choiceLA() {
    return new HSV(this.h, 0.05, 0.95);
  }

  _choiceLB() {
    return new HSV(this.h, 0.1, 0.1);
  }

  complementaryColor() {
    const choiceA = this._choiceLA();
    const choiceB = this._choiceLB();
    const thisL = this.luminance();
    const choiceALuminance = choiceA.luminance();
    const choiceBLuminance = choiceB.luminance();
    const ratioA =
      thisL > choiceALuminance
        ? (thisL + 0.05) / (choiceALuminance + 0.05)
        : (choiceALuminance + 0.05) / (thisL + 0.05);
    const ratioB =
      thisL > choiceBLuminance
        ? (thisL + 0.05) / (choiceBLuminance + 0.05)
        : (choiceBLuminance + 0.05) / (thisL + 0.05);
    return ratioA > ratioB ? choiceA : choiceB;
  }
}

const randColor: () => HSVType = () =>
  new HSV(
    Math.floor(Math.random() * 360),
    Math.floor(Math.random() * 100) / 100,
    Math.floor(Math.random() * 100) / 100,
  );
export { randColor };
export default HSV;
