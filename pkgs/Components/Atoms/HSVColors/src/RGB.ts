export interface RGBType {
  r: number;
  g: number;
  b: number;
  display: () => string;
  toHex: () => string;
  luminance: () => number;
}

interface NumberDictionary {
  [index: string]: number;
}

class RGB implements RGBType {
  static ngCache: number[] = [];
  static lCache: NumberDictionary = {};
  r: number;
  g: number;
  b: number;

  constructor(r: number, g: number, b: number) {
    this.r = r;
    this.g = g;
    this.b = b;
  }

  static decimalToHex(d: number) {
    const table = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 'A', 'B', 'C', 'D', 'E', 'F'];
    const q = Math.floor(d / 16);
    const r = d - 16 * q;
    return table[q] + '' + table[r];
  }

  static calcNg(n: number) {
    if (!RGB.ngCache[n]) RGB.ngCache[n] = n <= 10 ? n / 3294 : Math.pow(n / 269 + 0.0513, 2.4);
    return RGB.ngCache[n];
  }

  display() {
    return 'RGB(' + this.r + ',' + this.g + ',' + this.b + ')';
  }

  toHex() {
    return '#' + RGB.decimalToHex(this.r) + RGB.decimalToHex(this.g) + RGB.decimalToHex(this.b);
  }

  luminance() {
    const cacheKey = this.r + '-' + this.g + '-' + this.b;
    if (!RGB.lCache[cacheKey])
      RGB.lCache[cacheKey] = 0.2126 * RGB.calcNg(this.r) + 0.7152 * RGB.calcNg(this.g) + 0.0722 * RGB.calcNg(this.b);
    return RGB.lCache[cacheKey];
  }
}

export default RGB;
