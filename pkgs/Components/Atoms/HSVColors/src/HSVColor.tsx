import * as React from 'react';
import { ReactNode } from 'react';
import { HSVType } from './HSV';

interface ColorProps {
  children?: ReactNode;
  color: HSVType;
}

const HSVColor: (props: ColorProps) => JSX.Element = (props) => {
  return (
    <div
      className="full-div"
      style={{
        backgroundColor: props.color ? props.color.toHex() : 'lightgray',
        color: props.color.complementaryColor().toHex(),
      }}
    >
      {props.children}
    </div>
  );
};
export default HSVColor;
