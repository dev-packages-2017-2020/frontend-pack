import * as React from 'react';
import { ReactNode } from 'react';

interface FirstMenuProps {
  children?: ReactNode;
}

const MainNavSecondMenu: (props: FirstMenuProps) => JSX.Element = (props) => {
  return <ul className="second-main-menu">{props.children}</ul>;
};
export default MainNavSecondMenu;
