import * as React from 'react';
import { ReactNode } from 'react';

interface FirstMenuProps {
  children?: ReactNode;
}

const MainNavFirstMenu: (props: FirstMenuProps) => JSX.Element = (props) => {
  return <ul className="first-main-menu">{props.children}</ul>;
};
export default MainNavFirstMenu;
