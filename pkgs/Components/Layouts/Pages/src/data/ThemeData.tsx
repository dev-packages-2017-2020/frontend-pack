const ThemeSizes = {
  LITTLE: 'little',
  DEFAULT: 'default',
  LARGE: 'large',
};

const ThemeColors = {
  DEFAULT_LIGHT: 'default-light',
  RAINBOW_LIGHT: 'rainbow-light',
  RAINBOW_DARK: 'rainbow-dark',
  MONO_ORANGE: 'mono-orange',
  LOW_SAT_PINK: 'low-sat-pink',
  LOW_SAT_PINK_JOY: 'low-sat-pink-joy',
  LIGHT_YELLOW_PING: 'light-yellow-pink',
  WHITE_AND_BLACK: 'white-and-black',
};

export type ThemeSizeType = typeof ThemeSizes[keyof typeof ThemeSizes];
export type ThemeColorType = typeof ThemeColors[keyof typeof ThemeColors];

export { ThemeSizes, ThemeColors };
