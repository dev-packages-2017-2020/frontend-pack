import * as React from 'react';
import { ReactNode } from 'react';
import { ThemeColorType, ThemeSizeType } from './data/ThemeData';

interface PageProps {
  children?: ReactNode;
  themeSize: ThemeSizeType;
  themeColor: ThemeColorType;
  navs: 1 | 2;
}

const Page: (props: PageProps) => JSX.Element = (props) => {
  return (
    <div id="site-content">
      <div className={props.themeSize + ' ' + props.themeColor}>
        <div id={props.navs === 1 ? 'one-bar-layout-container' : 'two-bar-layout-container'}>{props.children}</div>
      </div>
    </div>
  );
};

export default Page;
