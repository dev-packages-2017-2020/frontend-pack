import * as React from 'react';
import { ReactNode } from 'react';

interface MainNavProps {
  children?: ReactNode;
  navIndex: 1 | 2;
}

const MainNav: (props: MainNavProps) => JSX.Element = (props) => {
  return <nav className={'v-rectangle-60 bg-color-' + props.navIndex + ' main-nav'}>{props.children}</nav>;
};
export default MainNav;
