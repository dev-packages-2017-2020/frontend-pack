import * as React from 'react';
import { ReactNode } from 'react';

interface RootSectionProps {
  children?: ReactNode;
}

const RootSection: (props: RootSectionProps) => JSX.Element = (props) => {
  return (
    <div id="root-section" className="padding-40 bg-color-3">
      {props.children}
    </div>
  );
};
export default RootSection;
