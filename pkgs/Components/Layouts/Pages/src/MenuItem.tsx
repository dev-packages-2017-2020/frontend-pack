import * as React from 'react';
import { ReactNode } from 'react';

interface MenuItemProps {
  children?: ReactNode;
}

const MenuItem: (props: MenuItemProps) => JSX.Element = (props) => {
  return <li>{props.children}</li>;
};
export default MenuItem;
