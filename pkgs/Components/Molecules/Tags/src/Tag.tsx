import * as React from 'react';
import { HSVType } from 'lili-cdr-atom-hsv-colors';

interface TagProps {
  name: string;
  value: any;
  color: HSVType;
}

const Tag: (props: TagProps) => JSX.Element = (props) => {
  return (
    <div
      className="margin-bottom-right-5 font-courier font-size-8 padding-5"
      style={{
        backgroundColor: props.color ? props.color.toHex() : 'lightgray',
        color: props.color.complementaryColor().toHex(),
      }}
      title={props.name}
    >
      <span className="explain">{props.name}: </span>
      {props.value.toString()}
    </div>
  );
};

export default Tag;
