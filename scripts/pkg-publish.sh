#!/usr/bin/env bash

. env-local
docker-compose -p liliatomspkg run --rm $1 npm version patch
docker-compose -p liliatomspkg run --rm $1 npm publish