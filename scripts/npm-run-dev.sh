#!/usr/bin/env bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
cd $DIR

. env-local
docker-compose -p liliatomspkg run --rm atom-colors npm run build
docker-compose -p liliatomspkg run --rm atom-hsv-colors npm run build
docker-compose -p liliatomspkg run --rm atom-lorem npm run build
docker-compose -p liliatomspkg run --rm atom-random npm run build
docker-compose -p liliatomspkg run --rm atom-shapes npm run build
docker-compose -p liliatomspkg run --rm molecule-tags npm run build
docker-compose -p liliatomspkg run --rm layout-pages npm run build

docker-compose -p frontend-example run --rm frontend-example-npm npm run dev