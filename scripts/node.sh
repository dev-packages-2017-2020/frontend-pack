#!/usr/bin/env bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
cd $DIR

. env-local
docker-compose -p frontend-example run --rm frontend-example-npm node $@