#!/usr/bin/env bash

. env-local
sh pkg-publish.sh atom-colors
sh pkg-publish.sh atom-hsv-colors
sh pkg-publish.sh atom-lorem
sh pkg-publish.sh atom-random
sh pkg-publish.sh atom-shapes
sh pkg-publish.sh molecule-tags
sh pkg-publish.sh layout-pages