#!/usr/bin/env bash

. env-local
docker-compose -p frontend-example -f docker-compose.yml -f docker-compose.local.yml up -d frontend-example-apache-server