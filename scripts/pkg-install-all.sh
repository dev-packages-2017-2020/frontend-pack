#!/usr/bin/env bash

. env-local
sh pkg-install.sh atom-colors
sh pkg-install.sh atom-hsv-colors
sh pkg-install.sh atom-lorem
sh pkg-install.sh atom-random
sh pkg-install.sh atom-shapes
sh pkg-install.sh molecule-tags
sh pkg-install.sh layout-pages
sh pkg-install.sh bundle-main
