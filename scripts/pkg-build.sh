#!/usr/bin/env bash

. env-local
docker-compose -p liliatomspkg run --rm $1 npm run format
docker-compose -p liliatomspkg run --rm $1 npm run lint
docker-compose -p liliatomspkg run --rm $1 npm run build
