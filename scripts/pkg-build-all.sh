#!/usr/bin/env bash

. env-local
sh pkg-build.sh atom-colors
sh pkg-build.sh atom-hsv-colors
sh pkg-build.sh atom-lorem
sh pkg-build.sh atom-random
sh pkg-build.sh atom-shapes
sh pkg-build.sh molecule-tags
sh pkg-build.sh layout-pages
