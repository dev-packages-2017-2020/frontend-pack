import * as React from 'react';
import {Circle, HRectangle, Rectangle, ShapeSizes, ShapeSizeType, Square, VRectangle} from "lili-cdr-atom-shapes";

const IShapes: () => JSX.Element = () => {

    return (
        <article>
            <h1>ATOM - Shapes</h1>
            <h2>Circles</h2>
            <div className="flex-row-wrap inventory-wireframe">
                {ShapeSizes.map((size: ShapeSizeType) => (
                    <Circle size={size}>
                        <div className="full-div flex-vertical-center" title={size.toString()}>{size}</div>
                    </Circle>
                ))}
            </div>
            <h2>Squares</h2>
            <div className="flex-row-wrap inventory-wireframe">
                {ShapeSizes.map((size: ShapeSizeType) => (
                    <Square size={size}>
                        <div className="full-div flex-vertical-center" title={size.toString()}>{size}</div>
                    </Square>)
                )}
            </div>
            <h2>H-Rectangles</h2>
            <div className="flex-row-wrap inventory-wireframe">
                {ShapeSizes.map((size: ShapeSizeType) => (
                    <HRectangle size={size}>
                        <div className="full-div flex-vertical-center" title={size.toString()}>{size}</div>
                    </HRectangle>)
                )}
            </div>
            <h2>V-Rectangles</h2>
            <div className="flex-row-wrap inventory-wireframe">
                {ShapeSizes.map((size: ShapeSizeType) => (
                    <VRectangle size={size}>
                        <div className="full-div flex-vertical-center" title={size.toString()}>{size}</div>
                    </VRectangle>))}
            </div>
            <h2>Rectangles</h2>
            <div className="flex-row-wrap inventory-wireframe">
                {ShapeSizes.map((width: ShapeSizeType) => (
                    ShapeSizes.map((height: ShapeSizeType) => (
                        <Rectangle width={width} height={height}>
                            <div className="full-div flex-vertical-center"
                                 title={"w:" + width + " h:" + height}>w:{width} h:{height}</div>
                        </Rectangle>
                    ))
                ))}
            </div>
        </article>
    )
};
export default IShapes;