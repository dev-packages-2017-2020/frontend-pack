import * as React from 'react';
import {randomBool, randomFloat, randomInt} from "lili-cdr-atom-random";

const IRandom: () => JSX.Element = () => {

    return (
        <article>
            <h1>ATOM - Random</h1>
            <h2>Integer</h2>
            <p>{randomInt(50)}</p>
            <p>{randomInt(50)}</p>
            <p>{randomInt(50)}</p>
            <h2>Float</h2>
            <p>{randomFloat(50)}</p>
            <p>{randomFloat(50)}</p>
            <p>{randomFloat(50)}</p>
            <h2>Boolean</h2>
            <p>{randomBool() ? "TRUE" : "FALSE"}</p>
            <p>{randomBool() ? "TRUE" : "FALSE"}</p>
            <p>{randomBool() ? "TRUE" : "FALSE"}</p>
        </article>
    )
};
export default IRandom;