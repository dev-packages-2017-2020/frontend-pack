import * as React from 'react';
import {BasicColor, BackgroundColors, BackgroundColorType} from "lili-cdr-atom-colors";
import {ThemeColors, ThemeColorType} from "lili-cdr-layout-app";
import {Square} from "lili-cdr-atom-shapes";

const IColors: () => JSX.Element = () => {

    return (
        <article>
            <h1>ATOM - Colors</h1>
            {Object.values(ThemeColors).map((theme: ThemeColorType) => (
                <React.Fragment>
                    <h2>{theme}</h2>
                    <div className={"flex-row-wrap inventory-wireframe " + theme.toString()}>
                        {BackgroundColors.map((color: BackgroundColorType) => (
                            <Square size={100}>
                                <BasicColor color={color}>
                                    <div className="full-div flex-vertical-center"
                                         title={color.toString()}>{color}</div>
                                </BasicColor>
                            </Square>
                        ))}
                    </div>
                </React.Fragment>
            ))}
        </article>
    )
};
export default IColors;