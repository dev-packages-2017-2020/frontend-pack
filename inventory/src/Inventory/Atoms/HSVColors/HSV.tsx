import * as React from 'react';
import {HSVColor, randColor} from "lili-cdr-atom-hsv-colors";
import {Circle} from "lili-cdr-atom-shapes";

const IHSV: () => JSX.Element = () => {

    const genColorDemo: () => JSX.Element = () => {
        const color = randColor();
        return (
            <Circle size={200}>
                <HSVColor color={color}>
                    <div className={"full-div flex-vertical-center"} title={color.display()}>{color.display()}</div>
                </HSVColor>
            </Circle>
        );
    };

    const genColorDemos: (i: number) => JSX.Element[] = (i) => {
        const arr: JSX.Element[] = [];
        for (let j = 0; j < i; j++) {
            arr.push(genColorDemo());
        }
        return arr;
    }
    return (
        <article>
            <h1>ATOM - HSV Color</h1>
            <div className={"flex-row-wrap inventory-wireframe"}>
                {genColorDemos(100)}
            </div>
        </article>
    )
};
export default IHSV;