import * as React from 'react';
import {lorem} from "lili-cdr-atom-lorem";
import {randomInt} from "lili-cdr-atom-random";

const ILorem: () => JSX.Element = () => {

    return (
        <article>
            <h1>ATOM - Lorem</h1>
            <h2>Generate Words</h2>
            <p>{lorem.generateWords(randomInt(5))}</p>
            <p>{lorem.generateWords(randomInt(5))}</p>
            <p>{lorem.generateWords(randomInt(5))}</p>
            <h2>Generate Sentences</h2>
            <p>{lorem.generateSentences(randomInt(3))}</p>
            <p>{lorem.generateSentences(randomInt(3))}</p>
            <p>{lorem.generateSentences(randomInt(3))}</p>
            <h2>Generate Paragraphs</h2>
            <p>{lorem.generateParagraphs(randomInt(3))}</p>
            <p>{lorem.generateParagraphs(randomInt(3))}</p>
            <p>{lorem.generateParagraphs(randomInt(3))}</p>
        </article>
    )
};
export default ILorem;