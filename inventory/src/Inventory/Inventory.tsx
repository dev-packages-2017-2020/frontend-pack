import * as React from 'react';
import "./Inventory.scss";
import IShapes from "./Atoms/Shapes/Shapes";
import IColors from "./Atoms/Colors/Colors";
import IHSV from "./Atoms/HSVColors/HSV";
import ILorem from "./Atoms/Lorem/Lorem";
import ITags from "./Molecules/Tags/Tags";
import {
    MainNav,
    MainNavFirstMenu,
    MainNavSecondMenu,
    MenuItem,
    Page, RootSection,
    ThemeColorType,
    ThemeSizeType
} from "lili-cdr-layout-app";

const Inventory: () => JSX.Element = () => {
    const themeColor: ThemeColorType = "inventory";
    const themeSize: ThemeSizeType = "default";
    return (
        <Page navs={2} themeColor={themeColor} themeSize={themeSize}>
            <MainNav navIndex={1}>
                <MainNavFirstMenu>
                    <MenuItem>Co 1</MenuItem>
                    <MenuItem>Co 2</MenuItem>
                </MainNavFirstMenu>
                <h1>Hello Title</h1>
                <MainNavSecondMenu>
                    <MenuItem>Co 3</MenuItem>
                    <MenuItem>Co 4</MenuItem>
                </MainNavSecondMenu>
            </MainNav>
            <MainNav navIndex={2}>
                <MainNavFirstMenu>
                    <MenuItem>Co 5</MenuItem>
                    <MenuItem>Co 6</MenuItem>
                </MainNavFirstMenu>
                <h1>Hello Title 2</h1>
                <MainNavSecondMenu>
                    <MenuItem>Co 7</MenuItem>
                    <MenuItem>Co 8</MenuItem>
                </MainNavSecondMenu>
            </MainNav>

            <RootSection>
                <IShapes/>
                <IColors/>
                <IHSV/>
                <ILorem/>
                <ITags/>
            </RootSection>
        </Page>
    );
};

export default Inventory;
