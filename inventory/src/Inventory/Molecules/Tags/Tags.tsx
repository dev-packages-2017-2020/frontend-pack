import * as React from 'react';
import {randColor} from "lili-cdr-atom-hsv-colors";
import {lorem} from "lili-cdr-atom-lorem";
import {randomAny, randomInt} from "lili-cdr-atom-random";
import {Tag} from "lili-cdr-molecule-tags";

const ITags: () => JSX.Element = () => {

    const getTagDemo: () => JSX.Element = () => {
        const color = randColor();
        return (
            <Tag color={color} name={lorem.generateWords(randomInt(2))} value={randomAny()}/>
        );
    };

    const genTagDemos: (i: number) => JSX.Element[] = (i) => {
        const arr: JSX.Element[] = [];
        for (let j = 0; j < i; j++) {
            arr.push(getTagDemo());
        }
        return arr;
    }
    return (
        <article>
            <h1>MOLECULE - Tags</h1>
            <div className={"flex-row-wrap inventory-wireframe"}>
                {genTagDemos(100)}
            </div>
        </article>
    )
};
export default ITags;