# Basic usage

## Install Party

```bash
cd public
ln -s ../inventory/dist/static
ln -s ../inventory/dist/index.html

cd scripts
cp env-local-example env-local
sh npm.sh install
sh npm.sh run dev
sh run-local.sh
```

## Run Example site
- `sh npm.sh run dev` => build the example
- `sh run-local.sh` => visit http://inventory.dev.local/

## Build and Publish packages

- `sh pkg-install-all.sh`
- `sh pkg-build-all.sh`
- `sh pkg-publish-all.sh`

# Directories

- inventory : an example of using all packages components
- pkgs : all packages
- public : where the build is stores (and rendered by the server)
- scripts : all tools to build and run the project